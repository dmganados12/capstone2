// [SECTION] Dependencies and Modules
	const User = require('../models/User');
	const bcrypt = require('bcrypt');
	const auth = require('../auth');

// [SECTION] Functionalities - Create
	module.exports.UserRegistration = (reg) => {
		
		let fName = reg.firstName;
		let lName = reg.lastName;
		let email = reg.email;
		let passW = reg.password;

		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, 10)
		});
		return newUser.save().then((user, error) => {
			if (user) {
				return user;
			} else {
				return false
			}
		});
	}

	module.exports.userToken = (reqBody) => {
		return User.findOne({email: reqBody.email}).then(outcome => {
			let passW = reqBody.password;
			if (outcome === null) {
				return "Email doesn't exist";
			}else {
				const isMatched = bcrypt.compareSync(passW, outcome.password);
				if (isMatched) {
					let useData = outcome.toObject();
					return {accessToken: auth.userAccessToken(useData)};
				} else {
					return "Password doesn't match. Please check credentials."
				}
			}

		})
	};

// [SECTION] Functionalities - Retrieve
	module.exports.getUsers = (id) => {
		return User.findById(id).then(outcome => {
			return outcome;
		});
	};
	

// [SECTION] Functionalities - Update
	module.exports.asAdmin = (userId) => {
		let updates = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, error) => {
			if (admin) {
				return 'User is set as an admin';
			} else {
				return 'Changes failed'
			}
		});
	};

	module.exports.asNonAdmin = (userId) => {
		let updates = {
			isAdmin: false
		}
		return User.findByIdAndUpdate(userId, updates).then((user, err) => {
			if (user) {
				return 'User is set as non-admin'
			} else {
				return 'Changes failed'
			};
		});
	};