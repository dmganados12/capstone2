// [SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const cors = require('cors');
	const userRoutes = require('./routes/users')

// [SECTION] Environment Variable Setup
	dotenv.config();
	const port = process.env.PORT;
	const credential = process.env.MONGODB_URL;

// [SECTION] Server Setup
	const app = express();
	app.use(express.json());
	

// [SECTION] Database Connect
	mongoose.connect(credential);
	const caps = mongoose.connection;
	caps.once('open', () => console.log (`Connected to Atlas`));

// [SECTION] Server Routes 
	app.use('/users', userRoutes)

// [SECTION] Server Responses
	app.get('/', (req, res) => {
		res.send(`Project Deployed Successfully`)
	});
	app.listen(port, () => {
		console.log(`API is responding to port ${port}`);
	});
