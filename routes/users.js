// [SECTION] Dependecies and Modules
	const express = require('express');
	const controller = require('../controllers/users');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Routes - POST
	route.post('/', (req, res) => {
		let userInfo = req.body;
		controller.UserRegistration(userInfo).then(result => {
			res.send(result)
		});
	});

	route.post('/user-token', (req, res) => {
		let data = req.body;
		controller.userToken(data).then(result => {
			res.send(result);			
		})
	});

//  [SECTION] Routes - GET
	route.get('/users', auth.verify, (req, res) => {
		let userData = auth.decode(req.headers.authorization);
		let userId = userData.id;
		controller.getUsers(userId).then(outcome => {
			res.send(outcome);
		});
	});

// [SECTION] Routes - PUT
	route.put('/:userId/set-as-admin', auth.verify,(req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let id = req.params.userId;
		let isAdmin = payload.isAdmin;
		(isAdmin) ? controller.asAdmin(id).then(outcome => 
			res.send(outcome))
		: res.send('Unauthorized user')	
	});

	route.put('/:userId/set-as-user', auth.verify,(req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let id = req.params.userId;
		isAdmin ? controller.asNonAdmin(id).then(result => res.send(result))
		: res.send('Unauthorized user')
	});

// [SECTION] Expose Route System
module.exports = route;