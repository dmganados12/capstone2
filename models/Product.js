// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Schema
const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, 'Please indicate the Product Name']
	},
	description: {
		type: String,
		required: [true, 'Description is Required']
	},
	sellingPrice: {
		type: Number,
		required: [true, 'Selling Price is Required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
});

// [SECTION] Model
module.exports = mongoose.model('Product', productSchema);